
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

// require('dotenv').config();

const app = express();
const port = process.env.PORT || 5001;

app.use(cors());
app.use(express.json());

// const uri = process.env.ATLAS_URI;
const uri ="mongodb+srv://nikhilbelwate:Ganesha123@clusterecomm.tjlyvex.mongodb.net/300365367-nikhil"
//const uri = "mongodb+srv://newuser:123@cluster0.f9d6o.gcp.mongodb.net/activitiescollections";
mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true,  useUnifiedTopology: true   }
);
const connection = mongoose.connection;
connection.once('open', () => {
  console.log("MongoDB database connection established successfully");
})


// import routes
const bookRouter= require('./routes/books');


// adding / to before all bookRouter
app.use('/', bookRouter);

app.get('*', function(req, res){
  res.send('Page Not found / Server Error', 404);
});

app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});
