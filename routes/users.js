const router = require('express').Router();
let User = require('../models/user.model');

router.get("/",async function(req,res){
   
    User.find().then((users) => res.json(users))
    .catch((err) => res.status(400).json('Error: ' + err));
});

router.route('/add').post(async (req, res) => {
    const student = req.body.student;
    console.log(student)
    // create a new Activity object
    const newUser = new User(student);
  
    console.log(newUser);
    // save the new object (newActivity)
    await newUser.save()
      .then(() => res.send('newUser added!'))
      .catch((err) => res.status(400).json('Error: ' + err));
});
module.exports = router;

