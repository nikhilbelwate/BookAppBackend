const router = require('express').Router();
const bookList=require('../models/bookList.model')

router.route('/').get((req, res) => {
    bookList.find()
      .then((books) => res.json(books))
      .catch((err) => res.status(400).json('Error: ' + err));
  });

router.route('/add').post(async (req, res) => {
    const book = req.body;
    // create a new Activity object
    const newBook = await new bookList(book);
  
    console.log(newBook);
    // save the new object (newActivity)
    newBook
      .save()
      .then(() => res.json({message:'Book added successfully!'}))
      .catch((err) => res.status(400).json(err));
  });

router.route('/:id').get((req, res) => {
    bookList.findById(req.params.id)
      .then((books) => res.json(books))
      .catch((err) => res.status(400).send('Page Not found / Server Error'));
  });

router.route('/:id').delete(async (req, res) => {
  await bookList.findByIdAndDelete(req.params.id)
      .then(() => res.json({message:'Book deleted successfully!'}))
      .catch((err) => res.status(400).json(err));
  });

router.route('/:id').put(async (req, res) => {
    console.log(req.params.id);
    let updatesforbook= req.body;
    
    try{
        const result = await bookList.updateOne({_id:req.params.id},updatesforbook)
        
        res.json({message:'Book updated successfully!', modifiedCount: result.modifiedCount, matchedCount: result.matchedCount})
          
    }catch(e){
        res.status(400).json(err)
    }
      
  });


  module.exports = router;
